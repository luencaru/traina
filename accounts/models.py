# coding=utf-8
from __future__ import unicode_literals
from django.contrib.auth.models import PermissionsMixin
from django.contrib.postgres.fields import ArrayField, JSONField
from django.views.generic.dates import timezone_today
from django.conf import settings
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser,
                     **extra_fields):
        user = self.model(email=email, is_active=True,
                          is_staff=is_staff, is_superuser=is_superuser,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    MALE = "m"
    FEMALE = "f"
    email = models.EmailField(max_length=150, unique=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    gender = models.CharField(max_length=1, blank=True, choices=(
        (MALE, "male"), (FEMALE, "female")
    ))
    birthday = models.DateField(blank=True, null=True)
    facebook_link = models.URLField(blank=True, null=True)
    professional_career = models.CharField(max_length=150, blank=True, null=True)
    schedule = JSONField(default={"Lun": [], "Mar": [], "Mie": [], "Jue": [], "Vie": [], "Sab": [], "Dom": []}, )
    survival_highest = models.IntegerField(null=True, blank=True)
    survival_last = models.IntegerField(null=True, blank=True)
    miscellany_highest = models.IntegerField(null=True, blank=True)
    miscellany_last = models.IntegerField(null=True, blank=True)
    free_questions = ArrayField(models.IntegerField(), default=[], blank=True)
    experience = models.IntegerField(default=0, verbose_name='Experiencia')

    objects = UserManager()

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'

    def get_short_name(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"

    def get_user_face(self):
        if self.social_auth.count() > 0:
            return True
        else:
            return False

    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def total_wrongs(self):
        return self.history.filter(status='WRONG').count()

    def total_corrects(self):
        return self.history.filter(status='CORRECT').count()

    def total_blanks(self):
        return self.history.filter(status='BLANK').count()

    def is_premium(self):
        return PremiumCode.objects.filter(used_by=self).exists()

    def reached_limit(self):
        return len(self.free_questions) >= settings.MAX_FREE_QUESTIONS


class PremiumCode(models.Model):
    ALL = 'ALL'
    UNMSM = 'UNMSM'
    UNI = 'UNI'
    CALLAO = 'CALLAO'
    UNFV = 'UNFV'
    PUCP = 'PUCP'
    created_at = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=10, choices=(
        (ALL, u'Acceso total'),
        (UNMSM, u'Universidad Nacional Mayor de San Marcos'),
        (UNI, u'Universidad Nacional de Ingeniería'),
        (CALLAO, u'Universidad del Callao'),
        (UNFV, u'Universidad Federic Villareal'),
        (PUCP, u'Pontificia Universidad Católica del Perú')
    ), default=UNMSM)
    code = models.UUIDField(db_index=True)
    expiration_date = models.DateField(null=True, blank=True)
    used_at = models.DateTimeField(null=True)
    used_by = models.ForeignKey(User, null=True, blank=True, related_name='premium_codes')

    def is_lapsed(self):
        return self.expiration_date < timezone_today() if self.expiration_date else False

    @classmethod
    def user_premium(cls, user):
        return cls.objects.filter(used_by=user).exists()
