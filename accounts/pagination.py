from rest_framework.pagination import PageNumberPagination

__author__ = 'carlos'


class TenUserPagination(PageNumberPagination):
    page_size = 10
