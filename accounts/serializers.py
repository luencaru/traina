# coding=utf-8
from django.contrib.auth import authenticate

from rest_framework import serializers

from accounts.models import PremiumCode, User
# from badges.models import Badge
# from badges.serializers import SerializerBadgedInProfile
from badges.models import Level
from badges.serializers import SerializerLevelInProfile
from courses.models import Course, History
from courses.serializers import ListCourseSerializer


class UserProfileSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    email = serializers.EmailField(read_only=True)
    total_wrongs = serializers.IntegerField(read_only=True)
    total_corrects = serializers.IntegerField(read_only=True)
    total_blanks = serializers.IntegerField(read_only=True)
    is_premium = serializers.BooleanField(read_only=True)
    experience = serializers.IntegerField(read_only=True)
    level = serializers.SerializerMethodField(read_only=True)

    def get_level(self, obj):
        level = Level.objects.filter(is_enabled=True, min_experience__lte=obj.experience,
                                     max_experience__gte=obj.experience).first()
        if level:
            return SerializerLevelInProfile(level, context=self.context).data
        else:
            return None

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'gender', 'birthday', 'facebook_link',
                  'professional_career', 'survival_highest', 'survival_last', 'miscellany_highest', 'miscellany_last',
                  'total_wrongs', 'total_corrects', 'total_blanks', 'is_premium', 'experience', 'level')
        read_only_fields = ('facebook_link', 'survival_highest', 'miscellany_highest', 'experience')


class UserProfileScheduleUpdateRetrieve(serializers.ModelSerializer):
    schedule = serializers.DictField()
    courses = serializers.SerializerMethodField(read_only=True)

    def get_courses(self, obj):
        queryset = Course.objects.filter(is_enabled=True)
        return ListCourseSerializer(queryset, many=True, context=self.context).data

    class Meta:
        model = User
        fields = ('id', 'schedule', 'courses')

    def update(self, instance, validated_data):
        instance.schedule = validated_data.get('schedule')
        instance.save()
        return instance


class SetAnswersSerializer(serializers.Serializer):
    type = serializers.ChoiceField(choices=History.TYPE_CHOICES)
    wrongs = serializers.ListField(child=serializers.DictField())
    corrects = serializers.ListField(child=serializers.DictField())
    blanks = serializers.ListField(child=serializers.IntegerField())


class CreatePremiumCodesSerializer(serializers.Serializer):
    total = serializers.IntegerField(min_value=1)
    expiration_date = serializers.DateField(required=False, allow_null=True)
    type = serializers.ChoiceField(choices=(
        (u'Acceso total', PremiumCode.ALL),
        (u'Universidad Nacional Mayor de San Marcos', PremiumCode.UNMSM),
        (u'Universidad Nacional de Ingeniería', PremiumCode.UNI),
        (u'Universidad del Callao', PremiumCode.CALLAO),
        (u'Universidad Federic Villareal', PremiumCode.UNFV),
        (u'Pontificia Universidad Católica del Perú', PremiumCode.PUCP)
    ), default=PremiumCode.UNMSM)


class RegisterPremiumCodesSerializer(serializers.Serializer):
    code = serializers.UUIDField()

    def validate_code(self, value):
        self.instance = PremiumCode.objects.filter(code=value).first()
        if not self.instance:
            raise serializers.ValidationError(u"Este código no existe en nuestra plataforma")
        elif self.instance.used_by:
            raise serializers.ValidationError(u"Este código ya ha sido utilizado")
        elif self.instance.is_lapsed():
            raise serializers.ValidationError(u"Este código ya ha expirado")
        return value


class RegisterProfileSerializer(serializers.ModelSerializer):
    gender = serializers.CharField()
    birthday = serializers.DateField(allow_null=True, required=False)
    professional_career = serializers.CharField()

    class Meta:
        model = User
        fields = ('gender', 'birthday', 'professional_career')


class RegisterUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'password', 'last_name', 'email', 'gender', 'birthday',
                  'professional_career')
        write_only_fields = ('password',)

    def clean_password(self):
        if len(self.cleaned_data.get('password')) < 6:
            raise serializers.ValidationError(u"Mínimo 6 caracteres")
        else:
            return self.cleaned_data.get('password')

    def create(self, validated_data):
        user = User(first_name=validated_data.get('first_name'),
                    last_name=validated_data.get('last_name'), email=validated_data.get('email'),
                    gender=validated_data.get('gender'), birthday=validated_data.get('birthday'),
                    professional_career=validated_data.get('professional_career'))
        user.set_password(validated_data.get('password'))
        user.save()
        return user


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(error_messages={"blank": "Este campo es obligatorio"})
    password = serializers.CharField(error_messages={"blank": "Este campo es obligatorio"})

    def validate(self, attrs):
        self.user_cache = authenticate(username=attrs["username"], password=attrs["password"])
        if not self.user_cache:
            raise serializers.ValidationError("Invalid login")
        return attrs

    def get_user(self):
        return self.user_cache


class ListUserRankExperienceSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    email = serializers.EmailField(read_only=True)
    experience = serializers.IntegerField(read_only=True)
    level = serializers.SerializerMethodField(read_only=True)

    def get_level(self, obj):
        level = Level.objects.filter(is_enabled=True, min_experience__lte=obj.experience,
                                     max_experience__gte=obj.experience).first()
        if level:
            return SerializerLevelInProfile(level, context=self.context).data
        else:
            return None

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'experience', 'badge')
