from accounts.views import (RetrieveUpdateUserProfileAPI, FacebookLoginAPI, UpdateProfileScheduleAPI, SetAnswersAPI,
                            CreatePremiumCodesAPI, UseCodeAPI, UserRegisterAPI, LoginAPI, csv_report_files,
                            ListForExperienceAPIView)
from social.apps.django_app.views import complete
from django.conf.urls import url

urlpatterns = [
    url(r'^api/login/$', LoginAPI.as_view(), name="login"),
    url(r'^api/register/$', UserRegisterAPI.as_view(), name="register"),
    url(r'^api/me/$', RetrieveUpdateUserProfileAPI.as_view(), name="retrieve-profile"),
    url(r'^api/me/update/answers/$', SetAnswersAPI.as_view(), name="set-answers"),
    url(r'^api/me/update/schedules/$', UpdateProfileScheduleAPI.as_view(), name="update-schedule"),
    url(r'^api/login/(?P<backend>[^/]+)/$', FacebookLoginAPI.as_view(), name='facebook-login'),
    url(r'^api/complete/(?P<backend>[^/]+)/$', complete, name='facebook-complete'),
    url(r'^api/premium-codes/$', CreatePremiumCodesAPI.as_view(), name="create-premium-codes"),
    url(r'^api/me/use-code/$', UseCodeAPI.as_view(), name="use-code"),
    url(r'^user_report/$', csv_report_files, name="user-report"),
    url(r'^api/users/experience/$', ListForExperienceAPIView.as_view(), name="user-list-experience"),

]
