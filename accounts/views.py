import StringIO
import csv
import uuid
from wsgiref.util import FileWrapper
import zipfile
from django.conf import settings
from django.contrib.auth import authenticate
from django.db import transaction
from django.http import StreamingHttpResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.utils.timezone import now
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
import qrcode
from requests.exceptions import HTTPError
from rest_framework import status
from rest_framework import serializers
from rest_framework.authentication import SessionAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.generics import RetrieveUpdateAPIView, GenericAPIView, CreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.settings import api_settings
from rest_framework.views import APIView
from social.apps.django_app.utils import psa
from rest_framework.response import Response

from accounts.authentication import ExpiringTokenAuthentication
from accounts.mixins import TokenMixin, RefreshTokenMixin
from accounts.models import PremiumCode, User
from accounts.pagination import TenUserPagination
from accounts.serializers import (UserProfileSerializer, UserProfileScheduleUpdateRetrieve, SetAnswersSerializer,
                                  CreatePremiumCodesSerializer, RegisterPremiumCodesSerializer, RegisterUserSerializer,
                                  LoginSerializer, ListUserRankExperienceSerializer)
from courses.models import History


class FacebookLoginAPI(TokenMixin, APIView):
    @method_decorator(psa('accounts:facebook-complete'))
    def dispatch(self, request, *args, **kwargs):
        return super(FacebookLoginAPI, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        token = request.data.get('access_token')
        try:
            user = request.backend.do_auth(token)
        except HTTPError, ex:
            raise serializers.ValidationError(
                {api_settings.NON_FIELD_ERRORS_KEY: ["Invalid facebook token"]})
        token = self.regenerate(user)
        return Response({'token': token.key, 'id_user': user.id}, status=status.HTTP_200_OK)


class RetrieveUpdateUserProfileAPI(RefreshTokenMixin, RetrieveUpdateAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = UserProfileSerializer

    def get_object(self):
        return self.request.user

    def perform_update(self, serializer):
        validated_data = serializer.validated_data
        old = self.get_object()
        survival_last = validated_data.get("survival_last")
        survival_highest = old.survival_highest
        miscellany_last = validated_data.get("miscellany_last")
        miscellany_highest = old.miscellany_highest
        if survival_last and (not old.survival_highest or survival_last > old.survival_highest):
            survival_highest = survival_last
        if miscellany_last and (not old.miscellany_highest or miscellany_last > old.miscellany_highest):
            miscellany_highest = miscellany_last
        serializer.save(survival_highest=survival_highest, miscellany_highest=miscellany_highest)


class SetAnswersAPI(RefreshTokenMixin, GenericAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = SetAnswersSerializer

    def post(self, request, *args, **kwargs):
        serializer = SetAnswersSerializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        vd = serializer.validated_data
        History.objects.bulk_create([History(
            user=self.request.user,
            question_id=item.get("id"),
            answer=item.get("answer"),
            challenge=vd.get("challenge"),
            status=History.WRONG,
            type=vd.get("type")) for item in vd.get("wrongs")])
        History.objects.bulk_create([History(
            user=self.request.user,
            question_id=item.get("id"),
            challenge=vd.get("challenge"),
            answer=item.get("answer"),
            status=History.CORRECT,
            type=vd.get("type")) for item in vd.get("corrects")])
        History.objects.bulk_create([History(
            user=self.request.user,
            question_id=id,
            challenge=vd.get("challenge"),
            status=History.BLANK,
            type=vd.get("type")) for id in vd.get("blanks")])
        new_questions = set([item.get("id") for item in vd.get("wrongs")] + [item.get("id") for item in
                                                                             vd.get("corrects")] + vd.get("blanks"))

        total_questions = set(self.request.user.free_questions + list(new_questions))
        self.request.user.free_questions = list(total_questions)[:settings.MAX_FREE_QUESTIONS]
        self.request.user.save()
        return Response(status=status.HTTP_201_CREATED)


class UpdateProfileScheduleAPI(RetrieveUpdateAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = UserProfileScheduleUpdateRetrieve

    def get_object(self):
        return self.request.user

    def perform_update(self, serializer):
        obj = serializer.save(schedule=self.request.data.get('schedule'))


class CreatePremiumCodesAPI(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CreatePremiumCodesAPI, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        serializer = CreatePremiumCodesSerializer(data=request.GET)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        instances = []
        imf = StringIO.StringIO()
        myzip = zipfile.ZipFile(imf, 'w', zipfile.ZIP_DEFLATED)
        for number in xrange(validated_data.get("total")):
            code = uuid.uuid4()
            instances.append(PremiumCode(
                code=code,
                expiration_date=validated_data.get('expiration_date'),
                type=validated_data.get('type')))
            qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.ERROR_CORRECT_L,
                box_size=10,
                border=4
            )
            qr.add_data(unicode(code))
            qr.make(fit=True)
            img = qr.make_image()
            buffer = StringIO.StringIO()
            img.save(buffer)
            myzip.writestr(u"{}.png".format(code.hex), buffer.getvalue())
        myzip.close()
        imf.seek(0)
        PremiumCode.objects.bulk_create(instances)
        response = StreamingHttpResponse(FileWrapper(imf), content_type="application/zip")
        response['Content-Disposition'] = "attachment; filename*=UTF-8''qrcodes.zip"
        return response


class UseCodeAPI(GenericAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    serializer_class = RegisterPremiumCodesSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.instance
        instance.used_by = self.request.user
        instance.used_at = now()
        instance.save()
        return Response({"status": 200}, status=status.HTTP_200_OK)


class UserRegisterAPI(CreateAPIView):
    authentication_classes = ()
    permission_classes = (AllowAny,)
    serializer_class = RegisterUserSerializer

    @method_decorator(transaction.atomic)
    def dispatch(self, request, *args, **kwargs):
        return super(UserRegisterAPI, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if not User.objects.filter(email=serializer.validated_data.get('email')).exists():
            user = self.perform_create(serializer)
            token, created = Token.objects.get_or_create(user=user)
            headers = self.get_success_headers(serializer.data)
            user_serializer = RegisterUserSerializer(user, context={"request": request})
            return Response({'token': token.key, 'user': user_serializer.data}, status=status.HTTP_200_OK,
                            headers=headers)
        else:
            return Response({"detail": 'Correo ya resgistrado'}, status=status.HTTP_303_SEE_OTHER)


class LoginAPI(TokenMixin, APIView):
    def post(self, request, *args, **kwargs):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.get_user()
        token = self.regenerate(user)
        return Response({'token': token.key}, status=status.HTTP_200_OK)


def csv_report_files(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="traina_emails.csv"'
    writer = csv.writer(response)
    headers = ['name']
    writer.writerow(headers)

    emails = User.objects.exclude(email='').values_list('email', flat=True)
    for url in emails:
        row = []
        for field in headers:
            if field in headers:
                val = url
                if callable(val):
                    val = val()
                row.append(val)
        writer.writerow([s.encode("utf-8") for s in row])
    return response


class ListForExperienceAPIView(ListAPIView):
    authentication_classes = ()
    serializer_class = ListUserRankExperienceSerializer
    permission_classes = (AllowAny,)
    queryset = User.objects.all().order_by('experience')
    pagination_class = TenUserPagination
