__author__ = 'kodevian-6'
from django.conf.urls import url, include
from app.views import *

urlpatterns = [
    url(r'^index/', archive, name='archive'),
    url(r'^api-token-auth/', 'rest_framework.authtoken.views.obtain_auth_token', name="logeo"),

]
