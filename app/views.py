from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse

# Create your views here.
def archive(request):
    mi_template = loader.get_template("index.html")
    return HttpResponse(mi_template.render())