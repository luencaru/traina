from django.contrib import admin

# Register your models here.
from badges.models import Level

admin.site.register(Level)
