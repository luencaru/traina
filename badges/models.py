# coding=utf-8
from django.db import models


# Create your models here.
from accounts.models import User
from courses.models import Course


class Level(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, verbose_name='Nombre del Nivel')
    logo = models.ImageField(upload_to='levels/logo', null=True, blank=True)
    is_enabled = models.BooleanField(verbose_name='Habilitar', default=True)
    min_experience = models.IntegerField(verbose_name='Experiencia mínima')
    max_experience = models.IntegerField(verbose_name='Experiencia máxima')

    def __unicode__(self):
        return u'{} entre {} - {}'.format(self.name, self.min_experience, self.max_experience)

    class Meta:
        ordering = ("-created_at",)


class Badge(models.Model):
    PRACTICE = "PRACTICE"
    SURVIVAL = "SURVIVAL"
    MISCELLANY = "MISCELLANY"
    TYPE_CHOICES = (
        (PRACTICE, u'Practice mode'),
        (SURVIVAL, u'Survival mode'),
        (MISCELLANY, u'Miscellany mode')
    )
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, verbose_name='Nombre del badge')
    logo = models.ImageField(upload_to='badges/logo', null=True, blank=True)
    is_enabled = models.BooleanField(verbose_name='Habilitar', default=True)
    course = models.ForeignKey(Course, related_name='badge', null=True, blank=True)
    all_courses = models.BooleanField(default=False)
    min_questions = models.IntegerField(verbose_name='Cantidad mínima de preguntas del curso', blank=True)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES, blank=True)
    min_quantity_challenge = models.IntegerField(verbose_name='Cantidad mínima por Reto', blank=True)
    user = models.ManyToManyField(User, related_name='badges', through='UserBadge')

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ("-created_at",)


class UserBadge(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name='userbadges', on_delete=models.CASCADE)
    badge = models.ForeignKey(Badge, related_name='userbadges', on_delete=models.CASCADE)

    def __unicode__(self):
        return u'User:{} -Badge:{}'.format(self.user, self.badge)

    class Meta:
        ordering = ("-created_at",)
