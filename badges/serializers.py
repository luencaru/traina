# coding=utf-8
from rest_framework import serializers
from badges.models import Level

__author__ = 'carlos'


class SerializerLevel(serializers.ModelSerializer):
    class Meta:
        model = Level
        fields = ('id', 'name', 'logo', 'min_experience', 'max_experience')


class SerializerLevelInProfile(serializers.ModelSerializer):
    class Meta:
        model = Level
        fields = ('name', 'logo')
