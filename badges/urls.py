from badges.views import ListLevelAPIVIew

__author__ = 'carlos'
from django.conf.urls import url

urlpatterns = [
    url(r'^api/levels/$', ListLevelAPIVIew.as_view(), name="list-levels"),
]
