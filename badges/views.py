# coding=utf-8
from rest_framework.authentication import SessionAuthentication
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from accounts.authentication import ExpiringTokenAuthentication
from badges.models import Level
from badges.serializers import SerializerLevel


class ListLevelAPIVIew(ListAPIView):
    serializer_class = SerializerLevel
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Level.objects.filter(is_enabled=True)
