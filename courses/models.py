# coding=utf-8
from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.fields.array import ArrayField
from django.db import models
from accounts.models import User


class Course(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de Ingreso')
    last_modified = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')
    is_enabled = models.BooleanField(default=True)
    logo = models.ImageField(upload_to='courses/logo', null=True, blank=True)
    name = models.CharField(max_length=400)
    count_questions = models.IntegerField(default=0)
    identify = models.CharField(max_length=400, unique=True)
    cover = models.ImageField(upload_to='courses/cover', null=True, blank=True)

    def __unicode__(self):
        return self.name


class Topic(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de Ingreso')
    last_modified = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')
    is_enabled = models.BooleanField(default=True)
    name = models.CharField(max_length=250)
    course = models.ForeignKey(Course, related_name='topics')
    frequency = models.IntegerField(default=0)
    order_customize = models.IntegerField(default=0)
    descript_orden_custumizer = models.CharField(max_length=180, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Tema"
        verbose_name_plural = "Temas"
        ordering = ['order_customize']

    def countquestions(self):
        return self.questions.all().count()


class Question(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de Ingreso')
    last_modified = models.DateTimeField(auto_now=True, verbose_name='Fecha de modificacion')
    text = models.CharField(max_length=600)
    options = JSONField(verbose_name='opciones', default={})
    topic = models.ForeignKey(Topic, related_name='questions')
    date_show = models.CharField(max_length=100, blank=True)
    year_show = models.IntegerField(default=0, verbose_name='Año de pregunta')
    argument_answer = models.TextField(blank=True, verbose_name='Argumento de la respuesta')
    bookmarked_by = ArrayField(models.IntegerField(), default=[], blank=True)
    verified = models.BooleanField(default=True)
    source_number = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return self.text

    class Meta:
        verbose_name = "Pregunta"
        verbose_name_plural = "Preguntas"


class ChallengeTimeOut(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name='challengestimeout')
    questions_saved = JSONField(default=[], blank=True)  # {"id":id,"marked":"a"}
    questions_requested = JSONField(default=[], blank=True)  # [id,id,...]
    courses_id = ArrayField(models.IntegerField(), default=[], blank=True)  # [id,id,...]
    score = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return u'{}'.format(self.id)


class History(models.Model):
    PRACTICE = "PRACTICE"
    SURVIVAL = "SURVIVAL"
    MISCELLANY = "MISCELLANY"
    CORRECT = "CORRECT"
    WRONG = "WRONG"
    BLANK = "BLANK"
    TYPE_CHOICES = (
        (PRACTICE, u'Practice mode'),
        (SURVIVAL, u'Survival mode'),
        (MISCELLANY, u'Miscellany mode')
    )
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, related_name='history', db_index=True)
    question = models.ForeignKey(Question, related_name='resolutions')
    answer = models.CharField(max_length=1, blank=True)
    ch_timeout = models.ForeignKey(ChallengeTimeOut, null=True, blank=True, related_name='history')
    status = models.CharField(max_length=8, choices=(
        (CORRECT, u'Correct answer'),
        (WRONG, u'Wrong answer'),
        (BLANK, u'Left blank')
    ), db_index=True)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES)
