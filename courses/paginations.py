from rest_framework.pagination import PageNumberPagination

__author__ = 'carlos'


class TenSetPagination(PageNumberPagination):
    page_size = 1