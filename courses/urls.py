from django.conf.urls import url
from courses.views import (ListCoursesAPI, ListTopicAPI, ListQuestionAPI, CreateQuestionAPI, DetailCourseAPI,
                           DetailTopicAPI, BookmarkQuestionAPI, ListQuestionByCourseAPI, ListQuestionMarkedCourseAPI,
                           CountQuestionsTopicAPI, ListAllQuestionsAPI, ListCoursesMarkedAPI, CreateChallengeAPI,
                           RetrievePageChallengeAPI, CreateChallengeMissAPI)

urlpatterns = [
    url(r'^api/courses/$', ListCoursesAPI.as_view(), name="courses"),
    url(r'^api/courses/(?P<pk>\d+)/$', DetailCourseAPI.as_view(), name="course-detail"),
    url(r'^api/courses/(?P<pk>\d+)/topics/$', ListTopicAPI.as_view(), name="topics"),
    url(r'^api/courses/topics/(?P<pk>\d+)/$', DetailTopicAPI.as_view(), name="topic-retrieve"),
    url(r'^api/courses/(?P<pk>\d+)/counts/$', CountQuestionsTopicAPI.as_view(), name="topic-counts-questions"),
    url(r'^api/courses/topics/(?P<pk>\d+)/questions/$', ListQuestionAPI.as_view(), name="questions"),
    url(r'^api/courses/(?P<pk>\d+)/questions/$', ListQuestionByCourseAPI.as_view(), name="questions"),
    url(r'^api/questions/(?P<pk>\d+)/bookmark/$', BookmarkQuestionAPI.as_view(), name="bookmark-question"),
    url(r'^api/questions/bookmark/courses/(?P<pk>\d+)/$', ListQuestionMarkedCourseAPI.as_view(),
        name="bookmark-question-list"),
    url(r'^api/courses/bookmarks/$', ListCoursesMarkedAPI.as_view(),
        name="bookmark-courses-list"),
    url(r'^api/challenges/$', CreateChallengeAPI.as_view(), name="create_challenge"),
    url(r'^api/challenges/(?P<pk>\d+)/$', RetrievePageChallengeAPI.as_view(), name="create_challenge"),
    url(r'^api/courses/questions/create/$', CreateQuestionAPI.as_view(), name="create_question"),
    url(r'^api/courses/questions/all/$', ListAllQuestionsAPI.as_view(), name="list-question-all"),
    url(r'^api/challenges/miss/$', CreateChallengeMissAPI.as_view(), name="create_challenge_miss"),
]
