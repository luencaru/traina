from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView, GenericAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from accounts.authentication import ExpiringTokenAuthentication
from accounts.mixins import RefreshTokenMixin
from courses.models import Course, Topic, Question, ChallengeTimeOut
from courses.serializers import (ListCourseSerializer, ListTopicSerializer, ListQuestionSerializer,
                                 CreateQuestionSerializer, DetailCourseSerializer, RetrieveTopicSerializer,
                                 DetailCourseALLSerializer, ListCoursesBookmarkedSerializer,
                                 ListCoursesMarkedSerializer, ChallengeSerializer, CreateChallengeMissSerializer)


class DetailCourseAPI(RetrieveAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = DetailCourseSerializer
    queryset = Course.objects.all()


class ListCoursesAPI(ListAPIView):
    serializer_class = ListCourseSerializer
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Course.objects.filter(is_enabled=True)


class DetailTopicAPI(RetrieveAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = RetrieveTopicSerializer
    queryset = Topic.objects.all()


class ListTopicAPI(ListAPIView):
    serializer_class = ListTopicSerializer
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        try:
            course = Course.objects.get(id=self.kwargs.get('pk'), is_enabled=True)
            return course.topics.filter(is_enabled=True).order_by('-name')
        except ObjectDoesNotExist:
            return None


class ListQuestionAPI(ListAPIView):
    serializer_class = ListQuestionSerializer
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        topic = get_object_or_404(Topic, id=self.kwargs.get('pk'))
        exclude = [int(x) for x in self.request.GET.get('exclude', '').split(",") if x.isdigit()]
        limit = self.request.GET.get('limit', '')
        queryset = topic.questions.exclude(id__in=exclude).order_by('?')
        if self.request.user.reached_limit():
            queryset = queryset.filter(id__in=self.request.user.free_questions)
        if limit and limit.isdigit() and int(limit) > 0:
            queryset = queryset[:limit]
        return queryset


class BookmarkQuestionAPI(GenericAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = ListQuestionSerializer

    def put(self, request, *args, **kwargs):
        question = get_object_or_404(Question, id=self.kwargs.get('pk'))
        bookmarked_by = question.bookmarked_by
        bookmarked_by.append(self.request.user.id)
        question.bookmarked_by = list(set(bookmarked_by))
        question.save()
        return Response(status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        question = get_object_or_404(Question, id=self.kwargs.get('pk'))
        bookmarked_by = question.bookmarked_by
        if self.request.user.id in bookmarked_by:
            bookmarked_by.remove(self.request.user.id)
        question.bookmarked_by = list(set(bookmarked_by))
        question.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CreateChallengeAPI(RefreshTokenMixin, CreateAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = ChallengeSerializer

    def perform_create(self, serializer):
        vd = serializer.validated_data
        if vd.get("type") == Challenge.MISCELLANY:
            queryset = Question.objects.filter(topic__course__in=vd.get("courses_id"))
        else:
            queryset = Question.objects.all()
        if self.request.user.reached_limit():
            queryset = queryset.filter(id__in=self.request.user.free_questions)
        queryset = queryset.order_by('?')[:8].values_list('id', flat=True)
        serializer.save(user=self.request.user, questions_id=list(queryset))


class RetrievePageChallengeAPI(RefreshTokenMixin, RetrieveAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = ChallengeSerializer
    queryset = ChallengeTimeOut.objects.all()

    def retrieve(self, request, *args, **kwargs):
        page = self.request.GET.get("page")
        page = int(page) if page and page.isdigit() else 1
        instance = self.get_object()
        questions = instance.questions_id
        if 8 * page > len(questions):
            if instance.type == 'MISCELLANY':
                queryset = Question.objects.filter(topic__course__in=instance.courses_id)
            else:
                queryset = Question.objects.all()
            if self.request.user.reached_limit():
                queryset = queryset.filter(id__in=self.request.user.free_questions)
            queryset = queryset.exclude(id__in=instance.questions_id).order_by('?')[:8].values_list('id', flat=True)
            instance.questions_id = list(set(instance.questions_id + list(queryset)))
            instance.save()
        serializer = self.get_serializer(instance, context={"page": page})
        return Response(serializer.data)


class CreateQuestionAPI(CreateAPIView):
    authentication_classes = ()
    permission_classes = (AllowAny,)
    serializer_class = CreateQuestionSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(status=status.HTTP_201_CREATED, headers=headers)


class ListQuestionByCourseAPI(ListAPIView):
    serializer_class = ListQuestionSerializer
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Question.objects.filter(topic__course__id=self.kwargs.get('pk')).order_by("created_at")


class ListQuestionMarkedCourseAPI(RetrieveAPIView):
    serializer_class = ListCoursesBookmarkedSerializer
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return get_object_or_404(Course, id=self.kwargs.get('pk'))

    def get_courses(self):
        topics_id = Question.objects.filter(bookmarked_by__contains=[self.request.user.id]).values_list('topic_id',
                                                                                                        flat=True)
        courses_id = Topic.objects.filter(id__in=topics_id).values_list('course_id', flat=True)
        return Course.objects.filter(id__in=courses_id).order_by('name')


class ListCoursesMarkedAPI(ListAPIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = ListCoursesMarkedSerializer

    def get_queryset(self):
        return Course.objects.filter(is_enabled=True).order_by('name')


class CountQuestionsTopicAPI(APIView):
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data = []
        course = get_object_or_404(Course, id=self.kwargs.get('pk'))
        if hasattr(course.logo, 'url'):
            url = course.logo
        else:
            url = None
        if hasattr(course.cover, 'url'):
            url_cover = course.cover
        else:
            url_cover = None
        data_course = {"id": course.id, "name": course.name, "cover": url_cover,
                       "identify": course.identify, "logo": url, "topics": data}
        for topic in course.topics.all():
            counts = {"id": 0, "name": topic.name, "number_questions": topic.countquestions(),
                      "frequency": topic.frequency, "70": 0, "80": 0, "90": 0, "00": 0, "10": 0, 'id': topic.id,
                      '70': topic.questions.filter(year_show__lt=1980).count(),
                      "descript_orden_custumizer": topic.descript_orden_custumizer,
                      '80': topic.questions.filter(year_show__gte=1980).filter(year_show__lt=1990).count(),
                      '90': topic.questions.filter(year_show__gte=1990).filter(year_show__lt=2000).count(),
                      '00': topic.questions.filter(year_show__gte=2000).filter(year_show__lt=2010).count(),
                      '10': topic.questions.filter(year_show__gte=2010).count()}
            data.append(counts)

        return Response({"course": data_course}, status=status.HTTP_200_OK)


class ListAllQuestionsAPI(ListAPIView):
    serializer_class = DetailCourseALLSerializer
    authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Course.objects.all()


class CreateChallengeMissAPI(RefreshTokenMixin, GenericAPIView):
    # authentication_classes = (ExpiringTokenAuthentication, SessionAuthentication)
    # permission_classes = (IsAuthenticated,)
    authentication_classes = ()
    permission_classes = (AllowAny,)
    serializer_class = CreateChallengeMissSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        vd = serializer.validated_data
        id_courses = [id.get('id') for id in vd.get('courses')]
        id_questions = []
        questions = Question.objects.none()
        for d in vd.get('courses'):
            questions = questions | Question.objects.filter(topic__course__id=d.get('id')).order_by('?')[
                                    :d.get('quantity')]
            id_questions.append(questions.values_list('id', flat=True))

        challenge = Challenge(user=self.request.user, type=Challenge.MISCELLANY, questions_id=id_questions,
                              courses_id=id_courses, time=vd.get('time', 0))
        challenge.save()
