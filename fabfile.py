from __future__ import print_function
from fabric.api import *
from fabric.colors import green

env.colorize_errors = True


def server():
    env.user = 'ubuntu'
    env.host_string = '45.55.250.12'
    env.password = 'coaching2015'


def deploy():
    server()
    home_path = "/home/ubuntu"
    print(green("Beginning Deploy:"))
    with cd("{}/traina".format(home_path)):
        run("git pull ")
        run("bower install")
        run("source {}/trainaEnv/bin/activate && pip install -r requirements.txt".format(home_path))
        run("source {}/trainaEnv/bin/activate"
            "&& python manage.py collectstatic --noinput --settings='traina.settings.production'".format(
            home_path))
        run("source {}/trainaEnv/bin/activate"
            "&& python manage.py migrate --settings='traina.settings.production'".format(home_path))
        sudo("service nginx restart", pty=False)
        sudo("supervisorctl restart gunicorn_traina", pty=False)
    print(green("Deploy Succesful :)"))
