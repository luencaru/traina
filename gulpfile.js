/**
 * Created by kodevian-2 on 17/03/15.
 */
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    mincss = require('gulp-minify-css'),
    uglifyjs = require('gulp-uglifyjs');

gulp.task('sass', function () {
    gulp.src(['static/sass/main.scss'])
        .pipe(sass())
        .pipe(mincss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('static/css/'));
});
var paths = {
    scripts: [
//        'static/js/jquery.js',
//        'static/js/perfect-scrollbar.js',
//        'static/js/app.js'
    ]
};
gulp.task('scripts', function () {
    gulp.src(paths.scripts).pipe(uglifyjs('all.min.js', {
            mangle: false
        }))
        .pipe(gulp.dest('static/js/build'));
});
gulp.task('watch', function () {
    gulp.watch('static/sass/*', ['sass']);
//    gulp.watch(paths.scripts, ['scripts']);
});
gulp.task('watch_scripts', function () {
    gulp.watch(paths.scripts, ['scripts']);
});

gulp.task('default', ['sass', 'scripts']);