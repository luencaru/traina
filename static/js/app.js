/**
 * Created by jefri on 24/07/15.
 */
var app = angular.module('app', ['restangular', 'ui.router','monospaced.qrcode']);
app.config(['$interpolateProvider', '$httpProvider', '$stateProvider', '$urlRouterProvider', function ($interpolateProvider, $httpProvider, $stateProvider, $urlRouterProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    var _origin = window.location.origin + '/static/';

    $stateProvider
        .state('app', {
          url: "",
          abstract: true
        })
        .state('app.simulacrum', {
            url: "/login",
            parent: "app",
            views: {
                "@": {
                    templateUrl: _origin + "partials/loginSimulacrum.html",
                    controller: 'loginSimulacrumController'
                }
            }
        })
        .state('app.exam', {
            url: "/exam",
            parent: "app",
            views: {
                "@": {
                    templateUrl: _origin + "partials/simulacrum.html",
                    controller: 'simulacrumController'
                }
            }
        })

        .state('questions', {
            url: "/questions",
            parent: "app",
            templateUrl: _origin + "partials/questions.html",
            controller: 'questionController',
            views: {
                "@": {
                    templateUrl: _origin + "partials/questions.html",
                    controller: 'questionController'
                }
            }
        })
        .state('verify', {
            url: "/verify/:idcourse",
            parent: "app",
            views: {
                "@": {
                    templateUrl: _origin + "partials/verify.html",
                    controller: 'verifyQuestionController'
                }
            }
        });
    $urlRouterProvider.otherwise("/login");

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

}]);
app.run(['$rootScope', 'Restangular', function ($rootScope, Restangular) {

    Restangular.setBaseUrl('/api');
    Restangular.setRequestSuffix('/');
    // ver al deployar
    var host=window.location.hostname,
        token='70c1b8d25d3dac472786fb36fd20e0912279b225';
    if(host=="localhost"){
	//3ead9fdc78fed62e79c978b08728478135a0e50c
        token='748481dc2ad654905f159e3edb0ccd05d2e7c99e'
    }
    Restangular.setDefaultHeaders({Authorization: 'token ' + token});
    //8a7fd6365b7d1ed0932c895e1906d13f4c2f68c6 <-- deployado1
    //748481dc2ad654905f159e3edb0ccd05d2e7c99e <-- deployado2

    //3ead9fdc78fed62e79c978b08728478135a0e50c <-- localhost
    $rootScope.static = window.location.origin + '/static/';
    $rootScope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
    $rootScope.Materialize = {
        materializeValue: false,
        modify: function (val) {
            var self = this;
            self.materializeValue = val;
        }
    };
    $rootScope.assignImage = function (curso) {
        switch (curso.identify) {
            case 'lenguaje':
                return 'img/civica.png';
            case 'literatura':
                return 'img/R.V.png';
            case 'psicologia':
                return 'img/sicologia.png';
            case 'histoPeru':
                return 'img/historia.png';
            case 'histoUniversal':
                return 'img/historia.png';
            case 'geografia':
                return 'img/trigonometria.png';
            case 'civica':
                return 'img/sicologia.png';
            case 'filosofia':
                return 'img/consejos.png';
            case 'economia':
                return 'img/economia.png';
            case 'biologia':
                return 'img/biologia.png';
        }
        //return curso.logo; // cuando esten los logos completos
        return 'img/civica.png';
    };
}]);

app.controller("mainController", ['$scope', 'api', '$timeout', function ($scope, api, $timeout) {
    //console.log("main");

}]);
app.controller("loginSimulacrumController", ['$scope', 'api', '$timeout','$state', function ($scope, api, $timeout,$state) {
    console.log("login");
    $scope.login={
        key:null,
        _contructor: function (){
            var self=this;
            $scope.Materialize.modify(true);
            self.showStaggeredList('#img-unmsm');
        },
        loginSend:function(val){
            console.log(val);
            if(val!=null && val!=undefined){
                $state.go('app.exam');
            }

        },
        showStaggeredList: function (selector) {
            var time = 0;
            $(selector).find('li').velocity(
                {translateY: "350px"},
                {duration: 0});

            $(selector).find('li').each(function () {
                $(this).velocity(
                    {opacity: "1", translateY: "0"},
                    {duration: 800, delay: time, easing: [60, 10]});
                time += 120;
            });
        }
    };
    $scope.login._contructor();
}]);
app.controller("simulacrumController", ['$scope', 'api', function ($scope, api) {
    $scope.simulacrum = {
        listKeys:[],
        courseSelected:null,
        _constructor: function () {
            var self=this;
            $scope.Materialize.modify(true);
            api.courses.get().then(function (res) {
                $scope.cursos = res;
                for (var i = 0; i < res.length; i++) {
                    $scope.cursos[i].img = $scope.assignImage(res[i]);
                }
                console.log(res);
            }, function (err) {
            });
            for(var i=0;i<100;i++){
                self.listKeys.push(i);
            }
        },
        runEfect: function (idetify){
            var self = this;
            $scope.efectFinish = true;
            $(idetify).perfectScrollbar();
            self.showStaggeredList(idetify);
        },
        showStaggeredList: function (selector) {
            var time = 0;
            $(selector).find('li').velocity(
                {translateX: "-280px"},
                {duration: 0});

            $(selector).find('li').each(function () {
                $(this).velocity(
                    {opacity: "1", translateX: "0"},
                    {duration: 800, delay: time, easing: [60, 10]});
                time += 120;
            });
        },
        openCourse: function (course) {
            var self=this;
            ///api/challenge/courses/{pk}/questions
            //$scope.user.idUser.toString()
            $scope.efectFinishQuestion=false;
            api.challenge.one('courses').post('questions', {id_user: 1, id_courses: course.id.toString()})
                .then(function (res) {
                    var i = 0, lengthRes = res.solution.questions.length;
                    $scope.QuestionsAux=[];
                    self.courseSelected=course;
                    $scope.id_solution_prev = res.solution.id;
                    if (lengthRes > 0) {
                        for (i = 0; i < lengthRes; i++) {
                            $scope.QuestionsAux[i] = {
                                text: res.solution.questions[i].text,
                                date_show: res.solution.questions[i].date_show,
                                answer: res.solution.questions[i].options.answer,
                                options: [
                                    {id: 'a', text: res.solution.questions[i].options.options.a},
                                    {id: 'b', text: res.solution.questions[i].options.options.b},
                                    {id: 'c', text: res.solution.questions[i].options.options.c},
                                    {id: 'd', text: res.solution.questions[i].options.options.d},
                                    {id: 'e', text: res.solution.questions[i].options.options.e}
                                ]
                            };
                        }
                    } else {
                    }
                }, function (err) {
                });
        }
    };
    $scope.simulacrum._constructor();
}]);
app.controller("questionController", ['$scope', 'api', '$timeout','$localstorage','$state', function ($scope, api, $timeout,$localstorage){
    //console.log("question");
    $scope.date_show = undefined;
    $scope.addQuestion = {
        question: {
            "text": "",
            "option_a": "",
            "option_b": "",
            "option_c": "",
            "option_d": "",
            "option_e": "",
            "date_show": "",
            "year_show": undefined,
            "source_number":null,
            "answer": "",
            "argument_answer": " ",
            "topic": null,
            "verified":true
        },
        count:{
          date:null,
          hours:0,
          day:0
        },
        registerSuccess: false,
        _constructor: function () {
            $scope.Materialize.modify(false);
            api.courses.get().then(function (res) {
                $scope.cursos = res;
                //console.log("d",res);
            }, function (err) {
            });
            var day=new Date(),dateDay;
            dateDay=day.getDate();

            if ($localstorage.getObject('count') === undefined) {
               $localstorage.setObject('count', {
                                                  date:null,
                                                  hours:0,
                                                  day:0
                });
            } else {

            }

            //console.log(dateDay);
        },
        verify:function(){
            this.question.verified=!this.question.verified;
        },
        changeCourse: function () {
            api.courses.one($scope.coursoId.toString()).one('topics').get().then(function (res) {
                $scope.temas = res;
            }, function (err) {
            });
        },
        selectTheme: function () {
            console.log($scope.temaId);
        },
        update_date_show: function () {
            if ($scope.addQuestion.question.year_show < 2004) {
                $scope.date_show = undefined;
            }
        },
        updateDateShowString:function(val){
            if(val=='1'){
                $scope.date_show='I';
            }
            if(val=='2'){
                $scope.date_show='II';
            }
        },
        sendQuestion: function () {
            console.log($scope.coursoId, $scope.temaId);
            if ($scope.coursoId == undefined || $scope.temaId == undefined ||
                ($scope.addQuestion.question.year_show > 2003 && $scope.date_show == undefined)) {
                //console.log("no enviar");
                $scope.alertVoidCourseTheme = true;
                $timeout(function () {
                    $scope.alertVoidCourseTheme = false;
                }, 750);
            } else {
                if (!angular.isUndefined($scope.addQuestion.question.year_show)) {
                    console.log($scope.date_show);
                    if ($scope.date_show == undefined) {
                        $scope.addQuestion.question.date_show = $scope.addQuestion.question.year_show.toString();
                    } else {
                        if ($scope.date_show == 'i') {
                            $scope.date_show = 'I'
                        }
                        if ($scope.date_show == 'ii') {
                            $scope.date_show = 'II'
                        }
                        $scope.addQuestion.question.date_show = $scope.addQuestion.question.year_show.toString() + "-" + $scope.date_show;
                    }
                    //console.log($scope.date_show, $scope.addQuestion.question.date_show);
                    $scope.addQuestion.question.topic = $scope.temaId;
                    $scope.addQuestion.registerSuccess = false;
                    api.courses.one('questions').post('create', $scope.addQuestion.question).then(function (res) {
                        $scope.addQuestion.question = {
                            "text": "",
                            "option_a": "",
                            "option_b": "",
                            "option_c": "",
                            "option_d": "",
                            "option_e": "",
                            "date_show": "",
                            "source_number":null,
                            "year_show": undefined,
                            "answer": "",
                            "argument_answer": "",
                            "topic": $scope.temaId,
                            "verified":true
                        };
                        $scope.addQuestion.registerSuccess = true;
                        $('#modal-success-add').modal('show');
                        $timeout(function () {
                            $('#modal-success-add').modal('hide');
                            $('#question-text').focus();
                        }, 1000)
                    }, function (err) {
                    })

                }
            }
        }

    };
    $scope.addQuestion._constructor();

}]);
app.controller("verifyQuestionController", ['$scope', 'api','$stateParams','$filter',  function ($scope, api,$stateParams,$filter) {
    console.log("verify");
    $scope.verify={
        _constructor:function () {
           //console.log($stateParams.idcourse);
           $scope.viewTable=[true,false,false,false,false,false,false,false,false];
           $scope.answerLetters=['a','b','c','d','e'];
           api.courses.one($stateParams.idcourse).one('topics').get().then(function (res) {
               //console.log("temas ",res);
                var temasAux =res;
                $scope.temas = $filter('orderBy')(temasAux, 'number_questions', true);
            }, function (err) {
            });
            //api/courses/:id/questions/
            api.courses.one($stateParams.idcourse).one('questions').get().then(function (res) {
               console.log("preg. ",res);

                $scope.preguntas = res;
            }, function (err) {
            });
           //api/courses/topics/(?P<pk>\d+)/questions/
           api.courses.one($stateParams.idcourse).get().then(function (res){
               console.log("eys ",res);
               $scope.curso=res;
           });
        },
        answerIncorrecy: function (val){
            return $scope.answerLetters.indexOf(val);
        }
    };
    $scope.verify._constructor();
}]);
//api/courses/:id/
app.factory('api', function (Restangular) {
    return {
        courses: Restangular.one('courses'),
        challenge: Restangular.one('challenge')
    }
});
app.directive('focusMe', function ($timeout) {
    return {
        scope: {
            focusMeIf: "="
        },
        link: function (scope, element, attrs) {
            if (scope.focusMeIf === undefined || scope.focusMeIf) {
                $timeout(function () {
                    element[0].focus();
                });
            }
        }
    };
});
app.factory('$localstorage', ['$window', function($window) {
    return {
      set: function(key, value) {
        $window.localStorage[key] = value;
      },
      get: function(key, defaultValue) {
        return $window.localStorage[key] || defaultValue;
      },
      setObject: function(key, value) {
        $window.localStorage[key] = JSON.stringify(value);
      },
      getObject: function(key) {
        return JSON.parse($window.localStorage[key] || '{}');
      }
    };
}]);
