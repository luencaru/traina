from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'trainaDB',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost'
    }
}

DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['compendium.kodevian.com']

ADMINS = (
    ('Eysenck Freddy Gomez Orihuela', 'eysenck@kodevian.com'),
)

# Email
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'jefri@kodevian.com'
EMAIL_HOST_PASSWORD = '30101991'
